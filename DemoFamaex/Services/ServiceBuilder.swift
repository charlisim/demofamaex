//
//  ServiceBuilder.swift
//  DemoFamaex
//
//  Created by Carlos on 17/6/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import Foundation

class ServiceBuilder{
    
    func provideOrdersService()->OrdersService{
        return LocalOrdersService()
    }
    
}
