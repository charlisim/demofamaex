//
//  OrdersService.swift
//  DemoFamaex
//
//  Created by Carlos on 17/6/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import Foundation

public protocol OrdersService{
    func save(service:ServiceOrder, complete:(_ success:Bool)->Void)
}

class LocalOrdersService:OrdersService{
    private var orders:[ServiceOrder] = []
    
    func save(service: ServiceOrder, complete: (Bool) -> Void) {
        orders.append(service)
        complete(true)
    }
}
