//
//  SuccessOrderViewController.swift
//  DemoFamaex
//
//  Created by Carlos on 17/6/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import UIKit


class SuccessOrderViewController:UIViewController, ViewSetupProtocol{
    // Views
    let successLabel:UILabel = UILabel()
    let containerStack:UIStackView = UIStackView()
    let serviceLabel:UILabel = UILabel()
    let whenLabel:UILabel = UILabel()
    let whereLabel:UILabel = UILabel()
    let orderDateLabel:UILabel = UILabel()
    
    
    // Variables
    let order:ServiceOrder
    
    init(order:ServiceOrder){
        self.order = order
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        self.setupConstraints()
        self.setupActions()
    }
    
    func setupView() {
        self.view.backgroundColor = UIColor.white
        self.view.addSubview(successLabel)
        self.view.addSubview(containerStack)
        self.containerStack.alignment = .top
        self.containerStack.axis = .vertical
        self.containerStack.distribution = .fill
        self.containerStack.spacing = 10
        containerStack.addArrangedSubview(serviceLabel)
        containerStack.addArrangedSubview(whenLabel)
        containerStack.addArrangedSubview(whereLabel)
        containerStack.addArrangedSubview(orderDateLabel)
        
        self.successLabel.text = "Enhorabuena tu servicio esta confirmado"
        if let service = self.order.service, let date = self.order.date, let place = self.order.location{
            self.whenLabel.text = "Fecha y hora: \(dateToDateTime(date: date))"
            self.serviceLabel.text = "Servicio: \(service.name)"
            self.orderDateLabel.text = "Fecha de creación del pedido: \(dateToDateTime(date: self.order.creationDate))"
            self.whereLabel.text = "Lugar: \(place)"
        }
        
    }
    func setupConstraints() {
        self.successLabel.translatesAutoresizingMaskIntoConstraints = false
        self.successLabel.topAnchor.constraint(equalTo: self.topLayoutGuide.bottomAnchor).isActive = true
        self.successLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 10).isActive = true
        self.successLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -10).isActive = true
        self.successLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        self.containerStack.translatesAutoresizingMaskIntoConstraints = false
        self.containerStack.topAnchor.constraint(equalTo: self.topLayoutGuide.bottomAnchor, constant: 60).isActive = true
        self.containerStack.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 10).isActive = true
        self.containerStack.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -10).isActive = true
        
    }
}
