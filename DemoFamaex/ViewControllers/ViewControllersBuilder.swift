//
//  ViewControllersBuilder.swift
//  DemoFamaex
//
//  Created by Carlos on 17/6/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import UIKit


class ViewControllersBuilder{
    let serviceBuilder:ServiceBuilder
    
    init(serviceBuilder:ServiceBuilder){
        self.serviceBuilder = serviceBuilder
    }
    
    func buildMainViewController()->UIViewController{
        let vc = ViewController(orderService: self.serviceBuilder.provideOrdersService(), vcBuilder: self)
        return vc
    }
    func buildSuccessViewController(order:ServiceOrder)->UIViewController{
        let vc = SuccessOrderViewController(order: order)
        return vc
    }
}
