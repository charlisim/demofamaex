//
//  ViewController.swift
//  DemoFamaex
//
//  Created by Carlos on 16/6/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class ViewController: UIViewController, ViewSetupProtocol, UITextFieldDelegate{
    //Views
    let scrollView:UIScrollView = UIScrollView()
    let container:UIStackView = UIStackView()
    let serviceTitle:UILabel = UILabel()
    let serviceTextField: UITextField = PaddingTextField(paddingX: 20)
    let whenTitle:UILabel = UILabel()
    let whenTextField: UITextField = PaddingTextField(paddingX: 20)
    let whereTitle:UILabel = UILabel()
    let whereTextField: UITextField = PaddingTextField(paddingX: 20)
    let sendButton: UIButton = UIButton()
    
    
    // Variables
    var order:ServiceOrder = ServiceOrder()
    let orderService:OrdersService
    let vcBuilder:ViewControllersBuilder
    
    init(orderService:OrdersService, vcBuilder: ViewControllersBuilder){
        self.orderService = orderService
        self.vcBuilder = vcBuilder
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        self.setupConstraints()
        self.setupActions()
    }
    
    func setupView() {
        self.title = "Demo Famaex"
        self.view.backgroundColor = UIColor.white
        self.view.addSubview(scrollView)
        self.view.addSubview(sendButton)
        whenTextField.delegate = self
        serviceTextField.delegate = self
        whereTextField.delegate = self
        
        self.scrollView.addSubview(container)
        self.container.addArrangedSubview(serviceTitle)
        self.container.addArrangedSubview(serviceTextField)
        self.container.addArrangedSubview(whenTitle)
        self.container.addArrangedSubview(whenTextField)
        self.container.addArrangedSubview(whereTitle)
        self.container.addArrangedSubview(whereTextField)
        
        self.container.alignment = .fill
        self.container.axis = .vertical
        self.container.distribution = .fill
        self.container.spacing = 10
        
        [serviceTitle, whenTitle, whereTitle].forEach { (label ) in
            label.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.title1)
        }
        [serviceTextField, whenTextField, whereTextField].forEach { (textField) in
            textField.layer.borderWidth = 1
            textField.layer.borderColor = UIColor.gray.cgColor
            
        }
        serviceTitle.text = NSLocalizedString("service_title", comment: "")
        whenTitle.text = NSLocalizedString("when_title", comment: "")
        whereTitle.text = NSLocalizedString("where_title", comment: "")
        
        
        sendButton.setTitleColor(UIColor.black, for: UIControlState.normal)
        sendButton.setTitle(NSLocalizedString("order_button_title", comment: ""), for: UIControlState.normal)
        self.scrollView.keyboardDismissMode = .interactive
        self.scrollView.bounces = true
    }
    
    func setupConstraints() {
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: self.topLayoutGuide.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: self.sendButton.topAnchor).isActive = true
        
        self.sendButton.translatesAutoresizingMaskIntoConstraints = false
        self.sendButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        self.sendButton.leadingAnchor.constraint(equalTo:  self.view.leadingAnchor).isActive = true
        self.sendButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        self.sendButton.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        container.translatesAutoresizingMaskIntoConstraints = false
        container.leadingAnchor.constraint(equalTo: self.scrollView.leadingAnchor, constant : 20).isActive = true
        container.trailingAnchor.constraint(equalTo: self.scrollView.trailingAnchor, constant: -20).isActive = true
        container.topAnchor.constraint(equalTo: self.scrollView.topAnchor).isActive = true
        container.bottomAnchor.constraint(equalTo: self.scrollView.bottomAnchor).isActive = true
        container.widthAnchor.constraint(equalTo: self.scrollView.widthAnchor, constant: -40).isActive = true
        
        serviceTextField.heightAnchor.constraint(equalToConstant: 45).isActive = true
        whereTextField.heightAnchor.constraint(equalTo:serviceTextField.heightAnchor).isActive = true
        whenTextField.heightAnchor.constraint(equalTo:serviceTextField.heightAnchor).isActive = true
        
    }
    func setupActions() {
        sendButton.addTarget(self, action: #selector(ViewController.orderAction), for: .touchUpInside)
    }
    fileprivate func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func orderAction(){
        self.order.location = self.whereTextField.text
        if self.order.isValid(){
            print("VALID")
            self.orderService.save(service: self.order, complete: { (success) in
                if (success){
                    let vc = self.vcBuilder.buildSuccessViewController(order: order)
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    showAlert(title: "Error", message: "No se ha podido procesar la orden")
                }
            })
        }else{
            showAlert(title: "Error", message: "Debes completar todos los campos")
        }
        print("ORDER")
    }
    func selectDate(date:Date){
        self.order.date = date
        self.whenTextField.text = dateToDateTime(date: date)
    }
    func selectWhen(){
        print("WHEN")
        ActionSheetDatePicker(title: "Selecciona una fecha", datePickerMode: UIDatePickerMode.dateAndTime, selectedDate:Date(), doneBlock: { (picker, selectedDate, origin) in
            if let date = selectedDate as? Date{
                self.selectDate(date: date)
            }
        }, cancel: { (picker) in
            
        }, origin: self.whereTextField ).show()
    }
    func selectService(){
        ActionSheetMultipleStringPicker.show(withTitle: "Seleccione servicio", rows: [
            ["Fontanería", "Climatización", "Albañilería"],
            
            ], initialSelection: [0], doneBlock: {
                picker, indexes, values in
                if let arrayValues = values as? Array<String>, let arrayIndex = indexes as? Array<Int>{
                    let service = Service(id: arrayIndex[0], name: arrayValues[0])
                    self.order.service = service
                    self.serviceTextField.text = service.name
                }
                return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: self.serviceTextField)
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if (textField == whenTextField){
            selectWhen()
            whenTextField.resignFirstResponder()
            return false
        }else if (textField == serviceTextField){
            selectService()
            whenTextField.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

