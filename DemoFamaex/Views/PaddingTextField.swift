//
//  PaddingTextField.swift
//  DemoFamaex
//
//  Created by Carlos on 17/6/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import UIKit

class PaddingTextField: UITextField {
    
    let paddingX: CGFloat
    let paddingY: CGFloat
    
    init(paddingX:CGFloat = 0, paddingY: CGFloat = 0 ){
        self.paddingX = paddingX
        self.paddingY = paddingY
        super.init (frame: CGRect.zero)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: paddingX, dy: paddingY)
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: paddingX, dy: paddingY)
    }

}
