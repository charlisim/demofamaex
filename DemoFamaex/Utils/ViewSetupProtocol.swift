//
//  ViewSetupProtocol.swift
//  DemoFamaex
//
//  Created by Carlos on 17/6/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import Foundation


public protocol ViewSetupProtocol{
    
    func setupView()
    func setupConstraints()
    func setupActions()
    
}

public extension ViewSetupProtocol{
    func setupActions() {}
}




