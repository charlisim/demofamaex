//
//  DateFormatter.swift
//  DemoFamaex
//
//  Created by Carlos on 17/6/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import Foundation


func dateToDateTime(date:Date)->String{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
    return dateFormatter.string(from: date)
}
