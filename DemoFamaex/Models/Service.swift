//
//  Service.swift
//  DemoFamaex
//
//  Created by Carlos on 17/6/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import Foundation

public struct Service{
    var id:Int
    var name:String
}
