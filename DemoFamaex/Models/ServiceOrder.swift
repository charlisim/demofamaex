//
//  ServiceOrder.swift
//  DemoFamaex
//
//  Created by Carlos on 17/6/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import Foundation

public struct ServiceOrder {
    var service:Service?
    var date:Date?
    var location:String?
    var creationDate: Date = Date()
    
    func isValid()->Bool{
        return service != nil && date != nil && location != nil && location?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty == false
    }
    
}
